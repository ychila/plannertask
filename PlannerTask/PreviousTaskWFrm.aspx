﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PreviousTaskWFrm.aspx.vb" Inherits="PlannerTask.PreviousTaskWFrm" %>

<%@ Register assembly="DevExpress.Web.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxPivotGrid.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPivotGrid" tagprefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <script type="text/javascript">
                        function OnComboBoxInit(s, e) {
                            s.GetMainElement().onclick = function () {
                                s.PerformCallback();
                            };


                        }

                        function OnEndCallback(s, e) {
                            //isBound = true;
                            s.ShowDropDown();
                        }
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type ="text/css">

        #Contenedor
        {
            width: 90%;
            margin: 0 auto;
          
        }

        .auto-style1 {
            width: 100%;
        }

.dxbButtonSys.dxbTSys
{
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;

	display: inline-table;
    border-spacing: 0;
    border-collapse: separate;
}
.dxbButtonSys /*Bootstrap correction*/
{
    -webkit-box-sizing: content-box;
    -moz-box-sizing: content-box;
    box-sizing: content-box;
}
.dxbButtonSys
{
	cursor: pointer;
	display: inline-block;
	text-align: center;
	white-space: nowrap;
}
.dxbButton_Metropolis
{
    color: #333333;
    border: 1px solid #c0c0c0;
    background-color: white;
    padding: 1px;
    font: 12px 'Segoe UI', Helvetica, 'Droid Sans', Tahoma, Geneva, sans-serif;
}
        .auto-style2 {
            width: 177px;
        }
        .auto-style3 {
            width: 178px;
        }
        
        .auto-style7 {
            width: 176px;
        }
        .auto-style8 {
            width: 77px;
        }
        .auto-style9 {
            width: 76px;
        }

        .auto-style10 {
            text-align: right;
        }
        .auto-style11 {
            width: 245px;
        }
        .auto-style12 {
            width: 41px;
        }
        .auto-style13 {
            text-align: center;
        }

        .auto-style14 {
            width: 269px;
        }

    </style> 
</head>
<body>
    
    <form id="form1" runat="server">
       
    <div id="Contenedor">
    
        <table class="auto-style1">
            
            <tr>
                
                <td>
                    
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                    
                </td>
            </tr>

            <tr>
                <td>
                    
                    
                    <dx:ASPxMenu ID="ASPxMenu1" runat="server" EnableTheming="True" Theme="Metropolis" Width="100%" Visible="False">
                        <Items>
                            <dx:MenuItem NavigateUrl="~/SchedulingWFrm.aspx" Text="Calendario">
                            </dx:MenuItem>
                            <dx:MenuItem NavigateUrl="~/SplashWFrm.aspx" Text="Salir">
                            </dx:MenuItem>
                        </Items>
                    </dx:ASPxMenu>
                </td>
            </tr>

            <tr>
                <td class="auto-style10">
                    
                    
                    <dx:ASPxButton ID="btnSalir" runat="server" Text="Salir" Theme="Metropolis" Width="95px">
                    </dx:ASPxButton>
                    
                    
                    <dx:ASPxButton ID="btnImportarExcel" runat="server" Text="Importar Excel" Theme="Metropolis">
                    </dx:ASPxButton>
                    <dx:ASPxButton ID="btnCalendario" runat="server" Text="Calendario" Theme="Metropolis" Width="95px">
                    </dx:ASPxButton>
                    
                    
                </td>
            </tr>

            <tr>
                <td>
                    
                    
                </td>
            </tr>

            <tr>
                <td>
                    
                    
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    
                    
                    
                    
                    
                    
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <table class="dxflInternalEditorTable_Metropolis">
                                <tr>
                                    <td class="auto-style3">
                                        <dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="Vista" Theme="Metropolis">
                                        </dx:ASPxLabel>
                                        <dx:ASPxComboBox ID="cmbVista" runat="server" AutoPostBack="True" EnableTheming="True" SelectedIndex="0" Theme="Metropolis" ValueType="System.Int32">
                                            <Items>
                                                <dx:ListEditItem Selected="True" Text="POR DIA" Value="0" />
                                                <dx:ListEditItem Text="POR SEMANA" Value="1" />
                                            </Items>
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td class="auto-style7">
                                        <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Unidad Negocio" Theme="Metropolis">
                                        </dx:ASPxLabel>
                                        <dx:ASPxComboBox ID="cmbUnidadNegocio" runat="server" DataSourceID="SqlDataSourceUnidadNegocio" EnableTheming="True" SelectedIndex="0" TextField="Nombre" Theme="Metropolis" ValueField="ID" ValueType="System.Int32">
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td class="auto-style2">
                                        <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Producto" Theme="Metropolis">
                                        </dx:ASPxLabel>
                                        <dx:ASPxComboBox ID="cmbProductoVegetal" runat="server" DataSourceID="SqlDataSourceProductoVegetal" EnableTheming="True" SelectedIndex="0" TextField="Nombre" Theme="Metropolis" ValueField="ID" ValueType="System.Int32">
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td class="auto-style8">
                                        <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Semana Ini" Theme="Metropolis">
                                        </dx:ASPxLabel>
                                        <dx:ASPxComboBox ID="cmbSemanaIni" runat="server" DataSourceID="SqlDataSourceSemIni" EnableTheming="True" SelectedIndex="0" TextField="Nombre" Theme="Metropolis" ValueField="ID" ValueType="System.Int32" Width="70px">
                                        <ClientSideEvents EndCallback="OnEndCallback" Init="OnComboBoxInit" />
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td class="auto-style9">
                                        <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Semana Fin" Theme="Metropolis">
                                        </dx:ASPxLabel>
                                        <dx:ASPxComboBox ID="cmbSemanaFin" runat="server" DataSourceID="SqlDataSourceSemFin" EnableTheming="True" SelectedIndex="0" TextField="Nombre" Theme="Metropolis" ValueField="ID" ValueType="System.Int32" Width="70px">
                                        <ClientSideEvents EndCallback="OnEndCallback" Init="OnComboBoxInit" />
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td class="auto-style3">
                                        <dx:ASPxLabel ID="lblActividad" runat="server" Text="Actividad" Theme="Metropolis">
                                        </dx:ASPxLabel>
                                        <dx:ASPxComboBox ID="cmbActividad" runat="server" DataSourceID="SqlDataSourceActividad" EnableTheming="True" SelectedIndex="0" TextField="Name" Theme="Metropolis" ValueField="ActivityGroupID" ValueType="System.Int32">
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    
                    
                    
                    
                    
                    
                </td>
            </tr>
            <tr>
                <td>
                    
                    
                    
                    
                    
                    
                </td>
            </tr>
            <tr>
                <td>
                    
                    
                    
                    
                    
                    
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <dx:ASPxButton ID="btnCargar" runat="server" Text="Cargar" Theme="Metropolis" Width="100%">
                    </dx:ASPxButton>
                </td>
            </tr>
            <tr>
                <td>
                    
                    
                </td>
            </tr>
            <tr>
                <td>
                    
                    
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <dx:ASPxPivotGrid ID="PVGridTask" runat="server" ClientIDMode="AutoID" DataSourceID="SqlDataSourceTasks" EnableTheming="True" Height="700px" Theme="SoftOrange" Width="100%">
                                <Fields>
                                    <dx:PivotGridField ID="fieldDate" AreaIndex="8" Caption="Fecha" FieldName="Date" Name="fieldDate" TotalCellFormat-FormatString="d" TotalCellFormat-FormatType="Custom" TotalValueFormat-FormatString="d" TotalValueFormat-FormatType="Custom" ValueFormat-FormatString="d" ValueFormat-FormatType="DateTime">
                                    </dx:PivotGridField>
                                    <dx:PivotGridField ID="fieldUnidadNegocio" AreaIndex="0" FieldName="UnidadNegocio" Name="fieldUnidadNegocio" Area="ColumnArea">
                                    </dx:PivotGridField>
                                    <dx:PivotGridField ID="fieldProducto" AreaIndex="0" FieldName="Producto" Name="fieldProducto">
                                    </dx:PivotGridField>
                                    <dx:PivotGridField ID="fieldTarea" Area="RowArea" AreaIndex="1" FieldName="Tarea" Name="fieldTarea">
                                    </dx:PivotGridField>
                                    <dx:PivotGridField ID="fieldBloque" AreaIndex="3" FieldName="Bloque" Name="fieldBloque">
                                    </dx:PivotGridField>
                                    <dx:PivotGridField ID="fieldCama" AreaIndex="4" FieldName="Cama" Name="fieldCama">
                                    </dx:PivotGridField>
                                    <dx:PivotGridField ID="fieldCantidad" Area="DataArea" AreaIndex="0" CellFormat-FormatString="N2" CellFormat-FormatType="Numeric" FieldName="Cantidad" Name="fieldCantidad" TotalCellFormat-FormatString="N2" TotalCellFormat-FormatType="Numeric" TotalValueFormat-FormatString="N2" TotalValueFormat-FormatType="Numeric" ValueFormat-FormatString="N2" ValueFormat-FormatType="Numeric">
                                    </dx:PivotGridField>
                                    <dx:PivotGridField ID="fieldSpan" AreaIndex="1" FieldName="Span" Name="fieldSpan">
                                    </dx:PivotGridField>
                                    <dx:PivotGridField ID="fieldActividad" Area="RowArea" AreaIndex="0" FieldName="Actividad" Name="fieldActividad">
                                    </dx:PivotGridField>
                                    <dx:PivotGridField ID="fieldSemana" Area="ColumnArea" AreaIndex="1" FieldName="Semana" Name="fieldSemana">
                                    </dx:PivotGridField>
                                    <dx:PivotGridField ID="fieldPerformance" Area="RowArea" AreaIndex="3" Caption="Rendimiento" FieldName="Performance" Name="fieldPerformance" TotalCellFormat-FormatString="N2" TotalCellFormat-FormatType="Numeric" TotalValueFormat-FormatString="N2" TotalValueFormat-FormatType="Numeric" ValueFormat-FormatString="N2" ValueFormat-FormatType="Numeric">
                                    </dx:PivotGridField>
                                    <dx:PivotGridField ID="fieldMinutes" AreaIndex="2" Caption="Minutos" FieldName="Minutes" Name="fieldMinutes">
                                    </dx:PivotGridField>
                                    <dx:PivotGridField ID="fieldRendimiento" Area="RowArea" AreaIndex="2" Caption="Ud. Rendimiento" FieldName="Rendimiento" Name="fieldRendimiento">
                                    </dx:PivotGridField>
                                    <dx:PivotGridField ID="fieldPersonas" Area="DataArea" AreaIndex="1" CellFormat-FormatString="N2" CellFormat-FormatType="Numeric" FieldName="Personas" Name="fieldPersonas" TotalCellFormat-FormatString="N2" TotalCellFormat-FormatType="Numeric" TotalValueFormat-FormatString="N2" TotalValueFormat-FormatType="Numeric" ValueFormat-FormatString="N2" ValueFormat-FormatType="Numeric">
                                    </dx:PivotGridField>
                                    <dx:PivotGridField ID="fieldPersonasXSemana" AreaIndex="5" Caption="Personas / Semana" CellFormat-FormatString="N2" CellFormat-FormatType="Numeric" FieldName="PersonasXSemana" Name="fieldPersonasXSemana" TotalCellFormat-FormatString="N2" TotalCellFormat-FormatType="Numeric" TotalValueFormat-FormatString="N2" TotalValueFormat-FormatType="Numeric" ValueFormat-FormatString="N2" ValueFormat-FormatType="Numeric">
                                    </dx:PivotGridField>
                                    <dx:PivotGridField ID="fieldVariedad" AreaIndex="6" FieldName="Variedad" Name="fieldVariedad">
                                    </dx:PivotGridField>
                                    <dx:PivotGridField ID="fieldHoras" AreaIndex="7" CellFormat-FormatString="N2" CellFormat-FormatType="Numeric" FieldName="Horas" Name="fieldHoras" TotalCellFormat-FormatString="N2" TotalCellFormat-FormatType="Numeric" TotalValueFormat-FormatString="N2" TotalValueFormat-FormatType="Numeric" ValueFormat-FormatString="N2" ValueFormat-FormatType="Numeric">
                                    </dx:PivotGridField>
                                </Fields>
                                <optionsdata dataprocessingengine="LegacyOptimized" />
                                <OptionsPager AlwaysShowPager="True" ColumnsPerPage="15" RowsPerPage="10">
                                    <Summary Visible="False" />
                            <%--<PageSizeItemSettings Visible="True"/>--%>
                                </OptionsPager>
                        <%--<OptionsPager RowsPerPage="25" ColumnsPerPage="15" Visible="False" />--%>
                                <OptionsView HorizontalScrollBarMode="Auto" HorizontalScrollingMode="Virtual" ShowColumnGrandTotals="False" ShowColumnTotals="False" VerticalScrollBarMode="Auto" VerticalScrollingMode="Virtual" />
                            </dx:ASPxPivotGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
               
            <tr>
                <td>
                    
                    <asp:SqlDataSource ID="SqlDataSourceTasks" runat="server" ConnectionString="<%$ ConnectionStrings:PlannerTask %>" SelectCommand="EXEC sp_GetInfoPlannerTask @prmSemanaINI, @prmSemanaFIN, @prmUnidadNegocio,      @prmActividad, @prmProductoVegetal, 0, 2">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="cmbSemanaIni" DefaultValue="0" Name="prmSemanaINI" PropertyName="Value" />
                            <asp:ControlParameter ControlID="cmbSemanaFin" DefaultValue="0" Name="prmSemanaFIN" PropertyName="Value" />
                            <asp:ControlParameter ControlID="cmbUnidadNegocio" DefaultValue="0" Name="prmUnidadNegocio" PropertyName="Value" />
                            <asp:ControlParameter ControlID="cmbActividad" DefaultValue="0" Name="prmActividad" PropertyName="Value" />
                            <asp:ControlParameter ControlID="cmbProductoVegetal" DefaultValue="0" Name="prmProductoVegetal" PropertyName="Value" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSourceUnidadNegocio" runat="server" ConnectionString="<%$ ConnectionStrings:PlannerTask %>" SelectCommand="   SELECT ID = 0, Nombre = 'TODAS' 
	UNION ALL 
	SELECT 
	   BusinessUnitID AS ID, 
	   Name AS Nombre 
	FROM BusinessUnit 
	WHERE ActiveFlag = 1  AND BusinessUnitID IN (571, 460,462,464,469,474,500,569,570) ORDER BY ID"></asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSourceProductoVegetal" runat="server" ConnectionString="<%$ ConnectionStrings:PlannerTask %>" SelectCommand="SELECT ID = 0, Nombre = 'TODAS' 
UNION ALL
SELECT PlantProductID AS ID, PlantProductName AS Nombre FROM PlantProducts
WHERE FlagActive = 1 
ORDER BY ID"></asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSourceSemIni" runat="server" ConnectionString="<%$ ConnectionStrings:PlannerTask %>" SelectCommand="SELECT ID, Nombre =CONVERT(VARCHAR(10),Anio) + CASE WHEN Nombre = '1' THEN '01'
                                                    WHEN Nombre = '2' THEN '02'
													WHEN Nombre = '3' THEN '03'
													WHEN Nombre = '4' THEN '04'
													WHEN Nombre = '5' THEN '05'
													WHEN Nombre = '6' THEN '06'
													WHEN Nombre = '7' THEN '07'
													WHEN Nombre = '8' THEN '08'
													WHEN Nombre = '9' THEN '09' ELSE Nombre END FROM Semanas WHERE Anio &gt;= YEAR(GETDATE()) ORDER BY Anio"></asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSourceSemFin" runat="server" ConnectionString="<%$ ConnectionStrings:PlannerTask %>" SelectCommand="SELECT ID, Nombre =CONVERT(VARCHAR(10),Anio) + CASE WHEN Nombre = '1' THEN '01'
                                                    WHEN Nombre = '2' THEN '02'
													WHEN Nombre = '3' THEN '03'
													WHEN Nombre = '4' THEN '04'
													WHEN Nombre = '5' THEN '05'
													WHEN Nombre = '6' THEN '06'
													WHEN Nombre = '7' THEN '07'
													WHEN Nombre = '8' THEN '08'
													WHEN Nombre = '9' THEN '09' ELSE Nombre END FROM Semanas WHERE Anio &gt;= YEAR(GETDATE()) ORDER BY Anio">
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSourceActividad" runat="server" ConnectionString="<%$ ConnectionStrings:PlannerTask %>" SelectCommand="SELECT -1 AS ActivityGroupID, Name = 'TODAS' UNION ALL SELECT ActivityGroupID, UPPER(Name) AS Nombre FROM Planner_ActivityGroup ORDER BY ActivityGroupID"></asp:SqlDataSource>
                    <dx:ASPxPopupControl ID="ASPxPopupControl1" runat="server" AllowDragging="True" AllowResize="True" HeaderText="Cargar Excel..." Height="150px" PopupAction="None" PopupAnimationType="None" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Theme="SoftOrange" Width="480px">
                        <ContentCollection>
<dx:PopupControlContentControl runat="server">
    <table class="dxflInternalEditorTable_Metropolis">
        <tr>
            <td class="auto-style12">&nbsp;</td>
            <td class="auto-style14">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="Archivo">
                </dx:ASPxLabel>
            </td>
            <td class="auto-style14">
                <asp:FileUpload ID="FileUpload1" runat="server" />
            </td>
            <td>
                <dx:ASPxButton ID="btnCargarExcel" runat="server" Text="Cargar" Theme="Metropolis" Width="90px">
                </dx:ASPxButton>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:GridView ID="gvImportExcel" runat="server" AllowPaging="True" Width="100%" Visible="False">
                </asp:GridView>
            </td>
        </tr>
    </table>
                            </dx:PopupControlContentControl>
</ContentCollection>
                        <Border BorderWidth="10px" />
                    </dx:ASPxPopupControl>
                    <dx:ASPxPopupControl ID="ASPxPopupControl2" runat="server" AllowDragging="True" AllowResize="True" HeaderText="Alerta..." Height="70px" PopupAction="None" PopupAnimationType="None" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Theme="SoftOrange" Width="350px">
                        <ContentCollection>
<dx:PopupControlContentControl runat="server">
    <table class="dxflInternalEditorTable_Metropolis">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style13">
                <dx:ASPxLabel ID="lblMensajeAlerta" runat="server" Theme="Metropolis">
                </dx:ASPxLabel>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
                            </dx:PopupControlContentControl>
</ContentCollection>
                        <Border BorderWidth="10px" />
                    </dx:ASPxPopupControl>
                </td>
            </tr>
               
        </table>
    
    </div>
    </form>

</body>
</html>
