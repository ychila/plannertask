﻿Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Data.PivotGrid
Imports DevExpress.Web.ASPxPivotGrid
Imports DevExpress.XtraPivotGrid

Public Class PreviousTaskWFrm
    Inherits System.Web.UI.Page
    Public FileName As String
    Public FilasDevueltas As String
    Public IDSemana As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        InicializarConexion()

        If (Not IsPostBack) Then

            ConsultaSemana()
            SqlDataSourceSemIni.FilterExpression = "ID = " & IDSemana.ToString & ""
            SqlDataSourceSemFin.FilterExpression = "ID = " & IDSemana.ToString & ""

        End If


    End Sub

    Private Sub ConsultaSemana()

        Dim strSQL As String = ""

        strSQL = "EXEC sp_getIDWeekPlannerTask"
        IDSemana = GetDatoEscalar(strSQL, strConexion)

    End Sub

    Protected Sub btnCargar_Click(sender As Object, e As EventArgs) Handles btnCargar.Click
        SqlDataSourceTasks.DataBind()

    End Sub
    Private Sub cmbVista_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbVista.SelectedIndexChanged
        If cmbVista.Value = 1 Then

            PVGridTask.Fields("Personas").Area = PivotArea.FilterArea
            PVGridTask.Fields("Fecha").Area = PivotArea.FilterArea
            PVGridTask.Fields("PersonasXSemana").Area = PivotArea.DataArea

        Else

            PVGridTask.Fields("PersonasXSemana").Area = PivotArea.FilterArea
            PVGridTask.Fields("Personas").Area = PivotArea.DataArea
            PVGridTask.Fields("Fecha").Area = PivotArea.ColumnArea


        End If
    End Sub

    Protected Sub btnCalendario_Click(sender As Object, e As EventArgs) Handles btnCalendario.Click

        Me.Session("SemIni") = cmbSemanaIni.Value
        Me.Session("SemFin") = cmbSemanaFin.Value
        Me.Session("Actividad") = cmbActividad.Value
        Me.Session("UN") = cmbUnidadNegocio.Value

        Me.Response.Redirect("SchedulingWFrm.aspx")
    End Sub

    Protected Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Response.Redirect("SplashWFrm.aspx")
    End Sub

    Protected Sub btnImportarExcel_Click(sender As Object, e As EventArgs) Handles btnImportarExcel.Click
        Me.ASPxPopupControl1.ShowOnPageLoad = True
    End Sub

    Protected Sub btnCargarExcel_Click(sender As Object, e As EventArgs) Handles btnCargarExcel.Click
        If FileUpload1.HasFile Then

            FileName = Path.GetFileName(FileUpload1.PostedFile.FileName)
            Dim Extension As String = Path.GetExtension(FileUpload1.PostedFile.FileName)
            Dim FolderPath As String = ConfigurationManager.AppSettings("FolderPath")

            Dim FilePath As String = Server.MapPath(FolderPath + FileName)
            FileUpload1.SaveAs(FilePath)
            CargaArchivoCSV(FileName, "tempUploadPlantingPlan")
            InsertaInfoFinalATablas()

        End If
    End Sub


    Private Sub CargaArchivoCSV(ByVal FileName As String, ByVal TablaCarga As String)

        Dim strsql As String = ""
        strsql = "EXEC UploadFilecsv '" & TablaCarga & "', '" & FileName & "'"
        GetExecComando(strsql, strConexion)

    End Sub

    Private Sub InsertaInfoFinalATablas()

        Dim strSQL As String = ""

        strSQL = "EXEC sp_UploadFilePlantingPlan"
        FilasDevueltas = GetDatoEscalar(strSQL, strConexion)


        If FilasDevueltas.Length > 0 Then


            lblMensajeAlerta.Text = FilasDevueltas.ToString
            Me.ASPxPopupControl2.ShowOnPageLoad = True


        End If

    End Sub

    Private Sub SqlDataSourceTasks_Selecting(sender As Object, e As SqlDataSourceSelectingEventArgs) Handles SqlDataSourceTasks.Selecting
        e.Command.CommandTimeout = 0
    End Sub

    Private Sub cmbSemanaIni_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbSemanaIni.SelectedIndexChanged

        Me.SqlDataSourceSemIni.ConnectionString = strConexion
        Me.SqlDataSourceSemIni.SelectCommand = "SELECT ID, Nombre =CONVERT(VARCHAR(10),Anio) + CASE WHEN Nombre = '1' THEN '01'
                                                    WHEN Nombre = '2' THEN '02'
													WHEN Nombre = '3' THEN '03'
													WHEN Nombre = '4' THEN '04'
													WHEN Nombre = '5' THEN '05'
													WHEN Nombre = '6' THEN '06'
													WHEN Nombre = '7' THEN '07'
													WHEN Nombre = '8' THEN '08'
													WHEN Nombre = '9' THEN '09' ELSE Nombre END FROM Semanas WHERE Anio >= YEAR(GETDATE()) ORDER BY Anio"
        Me.cmbSemanaIni.DataBind()
        Me.SqlDataSourceSemIni.DataBind()

    End Sub

    Private Sub cmbSemanaFin_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbSemanaFin.SelectedIndexChanged

        Me.SqlDataSourceSemFin.ConnectionString = strConexion
        Me.SqlDataSourceSemFin.SelectCommand = "SELECT ID, Nombre =CONVERT(VARCHAR(10),Anio) + CASE WHEN Nombre = '1' THEN '01'
                                                    WHEN Nombre = '2' THEN '02'
													WHEN Nombre = '3' THEN '03'
													WHEN Nombre = '4' THEN '04'
													WHEN Nombre = '5' THEN '05'
													WHEN Nombre = '6' THEN '06'
													WHEN Nombre = '7' THEN '07'
													WHEN Nombre = '8' THEN '08'
													WHEN Nombre = '9' THEN '09' ELSE Nombre END FROM Semanas WHERE Anio >= YEAR(GETDATE()) ORDER BY Anio"
        Me.cmbSemanaFin.DataBind()
        Me.SqlDataSourceSemFin.DataBind()

    End Sub
End Class