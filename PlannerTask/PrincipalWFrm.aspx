﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PrincipalWFrm.aspx.vb" Inherits="PlannerTask.PrincipalWFrm" %>

<%@ Register assembly="DevExpress.Web.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type ="text/css">
         #Contenedor
        {
            width: 90%;
            margin: 0 auto;
          
        }

        .auto-style1 {
            width: 100%;
        }

        .auto-style3 {
            width: 178px;
        }
        .auto-style4 {
            width: 76px;
        }
        .auto-style5 {
            width: 80px;
        }
        .auto-style6 {
            width: 177px;
        }
        .auto-style7 {
            width: 78px;
        }

    </style> 
</head>
<body>
    <form id="form1" runat="server">
    <div id="Contenedor">
    
        <table class="auto-style1">
            <tr>
                <td>
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                </td>
            </tr>
            <tr>
                <td>
                    <dx:ASPxMenu ID="ASPxMenu1" runat="server" EnableTheming="True" Theme="Metropolis" Width="100%">
                        <Items>
                            <dx:MenuItem NavigateUrl="~/PreviousTaskWFrm.aspx" Text="Visor Dinamico">
                            </dx:MenuItem>
                            <dx:MenuItem NavigateUrl="~/SplashWFrm.aspx" Text="Salir">
                            </dx:MenuItem>
                        </Items>
                    </dx:ASPxMenu>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Logo.jpg" />
                </td>
            </tr>
            <tr>
                <td>
                    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Bienvenido" Theme="Metropolis">
                    </dx:ASPxLabel>
                    <dx:ASPxLabel ID="lblUsuario" runat="server" Theme="Metropolis">
                    </dx:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table class="dxflInternalEditorTable_SoftOrange">
                        <tr>
                            <td class="auto-style6">
                                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Unidad Negocio" Theme="Metropolis">
                                </dx:ASPxLabel>
                                <dx:ASPxComboBox ID="cmbUnidadNegocio" runat="server" DataSourceID="SqlDataSourceUnidadNegocio" EnableTheming="True" SelectedIndex="0" TextField="Nombre" Theme="Metropolis" ValueField="ID" ValueType="System.Int32">
                                </dx:ASPxComboBox>
                            </td>
                            <td class="auto-style3">
                                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Producto" Theme="Metropolis">
                                </dx:ASPxLabel>
                                <dx:ASPxComboBox ID="cmbProductoVegetal" runat="server" AutoPostBack="True" DataSourceID="SqlDataSourceProductoVegetal" EnableTheming="True" SelectedIndex="0" TextField="Nombre" Theme="Metropolis" ValueField="ID" ValueType="System.Int32">
                                </dx:ASPxComboBox>
                            </td>
                            <td class="auto-style5">
                                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Semana Ini" Theme="Metropolis">
                                </dx:ASPxLabel>
                                <dx:ASPxComboBox ID="cmbSemanaIni" runat="server" DataSourceID="SqlDataSourceSemIni" EnableTheming="True" SelectedIndex="0" TextField="Nombre" Theme="Metropolis" ValueField="ID" ValueType="System.Int32" Width="70px">
                                </dx:ASPxComboBox>
                            </td>
                            <td class="auto-style7">
                                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="Año" Theme="Metropolis">
                                </dx:ASPxLabel>
                                <dx:ASPxComboBox ID="ASPxComboBox3" runat="server" AutoPostBack="True" DataSourceID="SqlDataSourceAnio" EnableTheming="True" SelectedIndex="0" TextField="Anio" Theme="Metropolis" ValueField="Anio" ValueType="System.Int32" Width="70px">
                                </dx:ASPxComboBox>
                            </td>
                            <td class="auto-style4">
                                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Semana Fin" Theme="Metropolis">
                                </dx:ASPxLabel>
                                <dx:ASPxComboBox ID="cmbSemanaFin" runat="server" DataSourceID="SqlDataSourceSemFin" EnableTheming="True" SelectedIndex="0" TextField="Nombre" Theme="Metropolis" ValueField="Nombre" Width="70px">
                                </dx:ASPxComboBox>
                            </td>
                            <td class="auto-style6">
                                <dx:ASPxLabel ID="lblActividad" runat="server" Text="Actividad" Theme="Metropolis">
                                </dx:ASPxLabel>
                                <dx:ASPxComboBox ID="cmbActividad" runat="server" DataSourceID="SqlDataSourceActividad" EnableTheming="True" SelectedIndex="0" TextField="Nombre" Theme="Metropolis" ValueField="ID" ValueType="System.Int32">
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="Variedad" Theme="Metropolis">
                                </dx:ASPxLabel>
                                <dx:ASPxComboBox ID="cmbVariedad" runat="server" DataSourceID="SqlDataSourceVariedad" EnableTheming="True" SelectedIndex="0" TextField="Nombre" Theme="Metropolis" ValueField="ID" ValueType="System.Int32">
                                </dx:ASPxComboBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <dx:ASPxButton ID="btnCargar" runat="server" Text="Cargar" Theme="Metropolis" Width="100%">
                    </dx:ASPxButton>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:SqlDataSource ID="SqlDataSourceTasks" runat="server" ConnectionString="<%$ ConnectionStrings:PlannerTask %>" SelectCommand="EXEC sp_GetInfoPlannerTask @prmSemanaINI, @prmAnio, @prmSemanaFIN, @prmUnidadNegocio,      @prmActividad, @prmProductoVegetal, @prmVariedad">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="cmbSemanaIni" DefaultValue="0" Name="prmSemanaINI" PropertyName="Value" />
                            <asp:ControlParameter ControlID="ASPxComboBox3" DefaultValue="0" Name="prmAnio" PropertyName="Value" />
                            <asp:ControlParameter ControlID="cmbSemanaFin" DefaultValue="0" Name="prmSemanaFIN" PropertyName="Value" />
                            <asp:ControlParameter ControlID="cmbUnidadNegocio" DefaultValue="0" Name="prmUnidadNegocio" PropertyName="Value" />
                            <asp:ControlParameter ControlID="cmbActividad" DefaultValue="0" Name="prmActividad" PropertyName="Value" />
                            <asp:ControlParameter ControlID="cmbProductoVegetal" DefaultValue="0" Name="prmProductoVegetal" PropertyName="Value" />
                            <asp:ControlParameter ControlID="cmbVariedad" DefaultValue="0" Name="prmVariedad" PropertyName="Value" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSourceUnidadNegocio" runat="server" ConnectionString="<%$ ConnectionStrings:PlannerTask %>" SelectCommand="    SELECT ID = 0, Nombre = 'TODAS' 
	UNION ALL 
	SELECT 
	   BusinessUnitID AS ID, 
	   Name AS Nombre 
	FROM BusinessUnit 
	WHERE ActiveFlag = 1 AND PlantingAvailable = 1 
	ORDER BY ID"></asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSourceProductoVegetal" runat="server" ConnectionString="<%$ ConnectionStrings:PlannerTask %>" SelectCommand="SELECT ID = 0, Nombre = 'TODAS' 
UNION ALL
SELECT PlantProductID AS ID, PlantProductName AS Nombre FROM PlantProducts
WHERE FlagActive = 1 
ORDER BY ID"></asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSourceSemIni" runat="server" ConnectionString="<%$ ConnectionStrings:PlannerTask %>" SelectCommand="SELECT ID, Nombre FROM Semanas WHERE Anio = YEAR(GETDATE()) AND CONVERT(VARCHAR(11),GETDATE(),101) BETWEEN FInicial AND FFinal"></asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSourceAnio" runat="server" ConnectionString="<%$ ConnectionStrings:PlannerTask %>" SelectCommand="SELECT DISTINCT Anio FROM Semanas WHERE Anio &gt;= YEAR(GETDATE())"></asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSourceSemFin" runat="server" ConnectionString="<%$ ConnectionStrings:PlannerTask %>" SelectCommand="SELECT Nombre FROM Semanas WHERE Anio = @prmAnio">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="ASPxComboBox3" DefaultValue="0" Name="prmAnio" PropertyName="Value" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSourceActividad" runat="server" ConnectionString="<%$ ConnectionStrings:PlannerTask %>" SelectCommand="SELECT ActivityID AS ID, UPPER(Name) AS Nombre FROM Planner_Activities ORDER BY Name"></asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSourceVariedad" runat="server" ConnectionString="<%$ ConnectionStrings:PlannerTask %>" SelectCommand="SELECT ID = 0, Nombre = 'TODAS'
UNION ALL
SELECT 
   ProductID AS ID,
   UPPER(Product) AS Nombre 
FROM Products
WHERE PlantProductID = @prmPlantProductID
ORDER BY ID">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="cmbProductoVegetal" DefaultValue="0" Name="prmPlantProductID" PropertyName="Value" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <dx:ASPxGridView ID="ASPxGridView1" runat="server" Theme="SoftOrange" Width="100%" AutoGenerateColumns="False" DataSourceID="SqlDataSourceTasks" KeyFieldName="TaskID">
                        <SettingsAdaptivity AdaptivityMode="HideDataCells">
                        </SettingsAdaptivity>
                        <SettingsPager PageSize="20">
                        </SettingsPager>
                        <SettingsEditing Mode="Batch">
                        </SettingsEditing>
                        <Settings ShowFooter="True" />
                        <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" AllowSelectSingleRowOnly="True" EnableRowHotTrack="True" />
                        <SettingsSearchPanel Visible="True" />
                        <Columns>
                            <dx:GridViewDataTextColumn FieldName="TaskID" ReadOnly="True" Visible="False" VisibleIndex="0">
                                <EditFormSettings Visible="False" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataDateColumn Caption="Fecha" FieldName="Date" VisibleIndex="2">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataDateColumn>
                            <dx:GridViewDataTextColumn Caption="U. N" FieldName="UnidadNegocio" ReadOnly="True" Visible="False" VisibleIndex="3">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Producto" ReadOnly="True" VisibleIndex="6">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Tarea" ReadOnly="True" Visible="False" VisibleIndex="5">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Bloque" ReadOnly="True" VisibleIndex="7">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Cama" ReadOnly="True" VisibleIndex="8">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Cantidad" VisibleIndex="9">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Span" ReadOnly="True" Visible="False" VisibleIndex="10">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Actividad" ReadOnly="True" Visible="False" VisibleIndex="4">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Semana" VisibleIndex="1">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <TotalSummary>
                            <dx:ASPxSummaryItem DisplayFormat="Registros: {0}" FieldName="Semana" ShowInColumn="Semana" SummaryType="Count" />
                        </TotalSummary>
                    </dx:ASPxGridView>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
