﻿Public Class PrincipalWFrm
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        InicializarConexion()


        If Not IsPostBack = True Then

            Me.lblUsuario.Text = Me.Session("NombreUsr")

        End If
    End Sub

    Protected Sub btnCargar_Click(sender As Object, e As EventArgs) Handles btnCargar.Click
        SqlDataSourceTasks.DataBind()
    End Sub

    Private Sub cmbProductoVegetal_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbProductoVegetal.SelectedIndexChanged
        cmbVariedad.SelectedIndex = 0
    End Sub
End Class