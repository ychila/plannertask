﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SchedulingWFrm.aspx.vb" Inherits="PlannerTask.SchedulingWFrm" %>

<%@ Register assembly="DevExpress.Web.ASPxScheduler.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxScheduler" tagprefix="dxwschs" %>
<%@ Register assembly="DevExpress.XtraScheduler.v17.1.Core, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraScheduler" tagprefix="cc1" %>

<%@ Register assembly="DevExpress.Web.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <script type="text/javascript">
                        function OnComboBoxInit(s, e) {
                            s.GetMainElement().onclick = function () {
                                s.PerformCallback();
                            };


                        }

                        function OnEndCallback(s, e) {
                            //isBound = true;
                            s.ShowDropDown();
                        }
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type ="text/css">

          #Contenedor
        {
            width: 100%;
            margin: 0 auto;
          
        }

        .auto-style1 {
            width: 100%;
        }
.dxflInternalEditorTable_Metropolis
{
    width: 100%;
}
        .dxeBase_Metropolis
{
    font: 12px 'Segoe UI', Helvetica, 'Droid Sans', Tahoma, Geneva, sans-serif;
    color: #333333;
}
.dxeTrackBar_Metropolis,
.dxeIRadioButton_Metropolis,
.dxeButtonEdit_Metropolis,
.dxeTextBox_Metropolis,
.dxeRadioButtonList_Metropolis,
.dxeCheckBoxList_Metropolis,
.dxeMemo_Metropolis,
.dxeListBox_Metropolis,
.dxeCalendar_Metropolis,
.dxeColorTable_Metropolis
{
    -webkit-tap-highlight-color: transparent;
}
.dxeTextBox_Metropolis,
.dxeButtonEdit_Metropolis,
.dxeIRadioButton_Metropolis,
.dxeRadioButtonList_Metropolis,
.dxeCheckBoxList_Metropolis
{
    cursor: default;
}
.dxeButtonEdit_Metropolis
{
    background-color: white;
    border: 1px solid #c0c0c0;
    width: 170px;
    font: 12px 'Segoe UI', Helvetica, 'Droid Sans', Tahoma, Geneva, sans-serif;
    border-collapse: separate;
    border-spacing: 0;
}

.dxeTextBoxDefaultWidthSys,
.dxeButtonEditSys 
{
    width: 170px;
}

noindex:-o-prefocus,
input[type="text"].dxeEditArea_Metropolis,
input[type="password"].dxeEditArea_Metropolis
{
    margin-top: 1px;
    margin-bottom: 0;
}
input[type="text"].dxeEditArea_Metropolis,
input[type="password"].dxeEditArea_Metropolis
{
    margin-top: 0;
    margin-bottom: 0;
}

.dxeMemoEditAreaSys, /*Bootstrap correction*/
input[type="text"].dxeEditAreaSys, /*Bootstrap correction*/
input[type="password"].dxeEditAreaSys /*Bootstrap correction*/
{
    display: block;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    box-shadow: none;
    -webkit-transition: none;
    -moz-transition: none;
    -o-transition: none;
    transition: none;
	-webkit-border-radius: 0px;
    -moz-border-radius: 0px;
    border-radius: 0px;
}
.dxeEditAreaSys,
.dxeMemoEditAreaSys, /*Bootstrap correction*/
input[type="text"].dxeEditAreaSys, /*Bootstrap correction*/
input[type="password"].dxeEditAreaSys /*Bootstrap correction*/
{
    font: inherit;
    line-height: normal;
    outline: 0;
}

input[type="text"].dxeEditAreaSys, /*Bootstrap correction*/
input[type="password"].dxeEditAreaSys /*Bootstrap correction*/
{
    margin-top: 0;
    margin-bottom: 0;
}
.dxeEditAreaSys,
input[type="text"].dxeEditAreaSys, /*Bootstrap correction*/
input[type="password"].dxeEditAreaSys /*Bootstrap correction*/
{
    padding: 0px 1px 0px 0px; /* B146658 */
}
.dxeButtonEdit_Metropolis .dxeEditArea_Metropolis
{
    background-color: white;
}
.dxeEditArea_Metropolis,
body input.dxeEditArea_Metropolis
{
    color: #333333;
}
.dxeEditArea_Metropolis
{
    border: 1px solid #A0A0A0;
}
.dxeEditAreaSys 
{
    border: 0px!important;
    background-position: 0 0; /* iOS Safari */
    -webkit-box-sizing: content-box; /*Bootstrap correction*/
    -moz-box-sizing: content-box; /*Bootstrap correction*/
    box-sizing: content-box; /*Bootstrap correction*/
}

.dxeButtonEditSys .dxeButton,
.dxeButtonEditSys .dxeButtonLeft
{
    line-height: 100%;
}

.dxeButtonEditButton_Metropolis
{
    padding: 0px 2px 0px 3px;
}
.dxeButtonEditButton_Metropolis,
.dxeCalendarButton_Metropolis,
.dxeSpinIncButton_Metropolis,
.dxeSpinDecButton_Metropolis,
.dxeSpinLargeIncButton_Metropolis,
.dxeSpinLargeDecButton_Metropolis,
.dxeColorEditButton_Metropolis
{
    background-color: white;
    vertical-align: middle;
    cursor: pointer;
    text-align: center;
    white-space: nowrap;
}
.dxEditors_edtError_Metropolis,
.dxEditors_edtCalendarPrevYear_Metropolis,
.dxEditors_edtCalendarPrevYearDisabled_Metropolis,
.dxEditors_edtCalendarPrevMonth_Metropolis,
.dxEditors_edtCalendarPrevMonthDisabled_Metropolis,
.dxEditors_edtCalendarNextMonth_Metropolis,
.dxEditors_edtCalendarNextMonthDisabled_Metropolis,
.dxEditors_edtCalendarNextYear_Metropolis,
.dxEditors_edtCalendarNextYearDisabled_Metropolis,
.dxEditors_edtCalendarFNPrevYear_Metropolis,
.dxEditors_edtCalendarFNNextYear_Metropolis,
.dxEditors_edtEllipsis_Metropolis,
.dxEditors_edtEllipsisDisabled_Metropolis,
.dxEditors_edtDropDown_Metropolis,
.dxEditors_edtDropDownDisabled_Metropolis,
.dxEditors_edtSpinEditIncrementImage_Metropolis,
.dxEditors_edtSpinEditIncrementImageDisabled_Metropolis,
.dxEditors_edtSpinEditDecrementImage_Metropolis,
.dxEditors_edtSpinEditDecrementImageDisabled_Metropolis,
.dxEditors_edtSpinEditLargeIncImage_Metropolis,
.dxEditors_edtSpinEditLargeIncImageDisabled_Metropolis,
.dxEditors_edtSpinEditLargeDecImage_Metropolis,
.dxEditors_edtSpinEditLargeDecImageDisabled_Metropolis
{
	display:block;
	margin:auto;
}
.dxEditors_edtDropDown_Metropolis
{
    background-position: -12px -214px;
    width: 10px;
    height: 14px;
}
.dxeButtonEditButtonHover_Metropolis .dxEditors_edtClear_Metropolis,
.dxeButtonEditButtonHover_Metropolis .dxEditors_edtDropDown_Metropolis,
.dxeButtonEditButtonHover_Metropolis .dxEditors_edtEllipsis_Metropolis,
.dxeButtonEditButtonPressed_Metropolis .dxEditors_edtDropDown_Metropolis,
.dxeButtonEditButtonPressed_Metropolis .dxEditors_edtEllipsis_Metropolis,
.dxEditors_caRefresh_Metropolis,
.dxEditors_edtBinaryImageDelete_Metropolis,
.dxEditors_edtBinaryImageOpenDialog_Metropolis,
.dxEditors_edtCalendarFNNextYear_Metropolis,
.dxEditors_edtCalendarFNPrevYear_Metropolis,
.dxEditors_edtCalendarNextMonth_Metropolis,
.dxEditors_edtCalendarNextMonthDisabled_Metropolis,
.dxEditors_edtCalendarNextYear_Metropolis,
.dxEditors_edtCalendarNextYearDisabled_Metropolis,
.dxEditors_edtCalendarPrevMonth_Metropolis,
.dxEditors_edtCalendarPrevMonthDisabled_Metropolis,
.dxEditors_edtCalendarPrevYear_Metropolis,
.dxEditors_edtCalendarPrevYearDisabled_Metropolis,
.dxEditors_edtClear_Metropolis,
.dxEditors_edtDETSClockFace_Metropolis,
.dxEditors_edtDETSHourHand_Metropolis,
.dxEditors_edtDETSMinuteHand_Metropolis,
.dxEditors_edtDETSSecondHand_Metropolis,
.dxEditors_edtDropDown_Metropolis,
.dxEditors_edtDropDownDisabled_Metropolis,
.dxEditors_edtEllipsis_Metropolis,
.dxEditors_edtEllipsisDisabled_Metropolis,
.dxEditors_edtError_Metropolis,
.dxEditors_edtSpinEditDecrementImage_Metropolis,
.dxEditors_edtSpinEditDecrementImageDisabled_Metropolis,
.dxEditors_edtSpinEditIncrementImage_Metropolis,
.dxEditors_edtSpinEditIncrementImageDisabled_Metropolis,
.dxEditors_edtSpinEditLargeDecImage_Metropolis,
.dxEditors_edtSpinEditLargeDecImageDisabled_Metropolis,
.dxEditors_edtSpinEditLargeIncImage_Metropolis,
.dxEditors_edtSpinEditLargeIncImageDisabled_Metropolis,
.dxEditors_edtTBDecBtn_Metropolis,
.dxEditors_edtTBDecBtnDisabled_Metropolis,
.dxEditors_edtTBDecBtnHover_Metropolis,
.dxEditors_edtTBDecBtnPressed_Metropolis,
.dxEditors_edtTBIncBtn_Metropolis,
.dxEditors_edtTBIncBtnDisabled_Metropolis,
.dxEditors_edtTBIncBtnHover_Metropolis,
.dxEditors_edtTBIncBtnPressed_Metropolis,
.dxEditors_edtTokenBoxTokenRemoveButton_Metropolis,
.dxEditors_edtTokenBoxTokenRemoveButtonDisabled_Metropolis,
.dxEditors_fcadd_Metropolis,
.dxEditors_fcaddhot_Metropolis,
.dxEditors_fcgroupaddcondition_Metropolis,
.dxEditors_fcgroupaddgroup_Metropolis,
.dxEditors_fcgroupand_Metropolis,
.dxEditors_fcgroupnotand_Metropolis,
.dxEditors_fcgroupnotor_Metropolis,
.dxEditors_fcgroupor_Metropolis,
.dxEditors_fcgroupremove_Metropolis,
.dxEditors_fcopany_Metropolis,
.dxEditors_fcopavg_Metropolis,
.dxEditors_fcopbegin_Metropolis,
.dxEditors_fcopbetween_Metropolis,
.dxEditors_fcopblank_Metropolis,
.dxEditors_fcopcontain_Metropolis,
.dxEditors_fcopcount_Metropolis,
.dxEditors_fcopend_Metropolis,
.dxEditors_fcopequal_Metropolis,
.dxEditors_fcopexists_Metropolis,
.dxEditors_fcopgreater_Metropolis,
.dxEditors_fcopgreaterorequal_Metropolis,
.dxEditors_fcopless_Metropolis,
.dxEditors_fcoplessorequal_Metropolis,
.dxEditors_fcoplike_Metropolis,
.dxEditors_fcopmax_Metropolis,
.dxEditors_fcopmin_Metropolis,
.dxEditors_fcopnotany_Metropolis,
.dxEditors_fcopnotbetween_Metropolis,
.dxEditors_fcopnotblank_Metropolis,
.dxEditors_fcopnotcontain_Metropolis,
.dxEditors_fcopnotequal_Metropolis,
.dxEditors_fcopnotlike_Metropolis,
.dxEditors_fcopsingle_Metropolis,
.dxEditors_fcopsum_Metropolis,
.dxEditors_fcoptypefield_Metropolis,
.dxEditors_fcoptypefieldhot_Metropolis,
.dxEditors_fcoptypevalue_Metropolis,
.dxEditors_fcoptypevaluehot_Metropolis,
.dxEditors_fcremove_Metropolis,
.dxEditors_fcremovehot_Metropolis,
.dxeFocused_Metropolis .dxeTBHSys .dxeFocusedMDHSys .dxEditors_edtTBMainDH_Metropolis,
.dxeFocused_Metropolis .dxeTBHSys .dxeFocusedMDHSys .dxEditors_edtTBMainDHHover_Metropolis,
.dxeFocused_Metropolis .dxeTBHSys .dxeFocusedMDHSys .dxEditors_edtTBMainDHPressed_Metropolis,
.dxeFocused_Metropolis .dxeTBHSys .dxeFocusedSDHSys .dxEditors_edtTBSecondaryDH_Metropolis,
.dxeFocused_Metropolis .dxeTBHSys .dxeFocusedSDHSys .dxEditors_edtTBSecondaryDHHover_Metropolis,
.dxeFocused_Metropolis .dxeTBHSys .dxeFocusedSDHSys .dxEditors_edtTBSecondaryDHPressed_Metropolis,
.dxeFocused_Metropolis .dxeTBVSys .dxeFocusedMDHSys .dxEditors_edtTBMainDH_Metropolis,
.dxeFocused_Metropolis .dxeTBVSys .dxeFocusedMDHSys .dxEditors_edtTBMainDHHover_Metropolis,
.dxeFocused_Metropolis .dxeTBVSys .dxeFocusedMDHSys .dxEditors_edtTBMainDHPressed_Metropolis,
.dxeFocused_Metropolis .dxeTBVSys .dxeFocusedSDHSys .dxEditors_edtTBSecondaryDH_Metropolis,
.dxeFocused_Metropolis .dxeTBVSys .dxeFocusedSDHSys .dxEditors_edtTBSecondaryDHHover_Metropolis,
.dxeFocused_Metropolis .dxeTBVSys .dxeFocusedSDHSys .dxEditors_edtTBSecondaryDHPressed_Metropolis,
.dxeSpinDecButtonHover_Metropolis .dxEditors_edtSpinEditDecrementImage_Metropolis,
.dxeSpinDecButtonPressed_Metropolis .dxEditors_edtSpinEditDecrementImage_Metropolis,
.dxeSpinIncButtonHover_Metropolis .dxEditors_edtSpinEditIncrementImage_Metropolis,
.dxeSpinIncButtonPressed_Metropolis .dxEditors_edtSpinEditIncrementImage_Metropolis,
.dxeSpinLargeDecButtonHover_Metropolis .dxEditors_edtSpinEditLargeDecImage_Metropolis,
.dxeSpinLargeDecButtonPressed_Metropolis .dxEditors_edtSpinEditLargeDecImage_Metropolis,
.dxeSpinLargeIncButtonHover_Metropolis .dxEditors_edtSpinEditLargeIncImage_Metropolis,
.dxeSpinLargeIncButtonPressed_Metropolis .dxEditors_edtSpinEditLargeIncImage_Metropolis,
.dxeTBHSys .dxEditors_edtTBMainDH_Metropolis,
.dxeTBHSys .dxEditors_edtTBMainDHDisabled_Metropolis,
.dxeTBHSys .dxEditors_edtTBMainDHHover_Metropolis,
.dxeTBHSys .dxEditors_edtTBMainDHPressed_Metropolis,
.dxeTBHSys .dxEditors_edtTBSecondaryDH_Metropolis,
.dxeTBHSys .dxEditors_edtTBSecondaryDHDisabled_Metropolis,
.dxeTBHSys .dxEditors_edtTBSecondaryDHHover_Metropolis,
.dxeTBHSys .dxEditors_edtTBSecondaryDHPressed_Metropolis,
.dxeTBVSys .dxEditors_edtTBMainDH_Metropolis,
.dxeTBVSys .dxEditors_edtTBMainDHDisabled_Metropolis,
.dxeTBVSys .dxEditors_edtTBMainDHHover_Metropolis,
.dxeTBVSys .dxEditors_edtTBMainDHPressed_Metropolis,
.dxeTBVSys .dxEditors_edtTBSecondaryDH_Metropolis,
.dxeTBVSys .dxEditors_edtTBSecondaryDHDisabled_Metropolis,
.dxeTBVSys .dxEditors_edtTBSecondaryDHHover_Metropolis,
.dxeTBVSys .dxEditors_edtTBSecondaryDHPressed_Metropolis
{
    
    background-repeat: no-repeat;
    background-color: transparent;
}
.dxEditors_edtError_Metropolis,
.dxEditors_edtCalendarPrevYear_Metropolis,
.dxEditors_edtCalendarPrevYearDisabled_Metropolis,
.dxEditors_edtCalendarPrevMonth_Metropolis,
.dxEditors_edtCalendarPrevMonthDisabled_Metropolis,
.dxEditors_edtCalendarNextMonth_Metropolis,
.dxEditors_edtCalendarNextMonthDisabled_Metropolis,
.dxEditors_edtCalendarNextYear_Metropolis,
.dxEditors_edtCalendarNextYearDisabled_Metropolis,
.dxEditors_edtCalendarFNPrevYear_Metropolis,
.dxEditors_edtCalendarFNNextYear_Metropolis,
.dxEditors_edtEllipsis_Metropolis,
.dxEditors_edtEllipsisDisabled_Metropolis,
.dxEditors_edtDropDown_Metropolis,
.dxEditors_edtDropDownDisabled_Metropolis,
.dxEditors_edtSpinEditIncrementImage_Metropolis,
.dxEditors_edtSpinEditIncrementImageDisabled_Metropolis,
.dxEditors_edtSpinEditDecrementImage_Metropolis,
.dxEditors_edtSpinEditDecrementImageDisabled_Metropolis,
.dxEditors_edtSpinEditLargeIncImage_Metropolis,
.dxEditors_edtSpinEditLargeIncImageDisabled_Metropolis,
.dxEditors_edtSpinEditLargeDecImage_Metropolis,
.dxEditors_edtSpinEditLargeDecImageDisabled_Metropolis
{
	display:block;
	margin:auto;
}

        .auto-style7 {
            width: 176px;
        }
        .auto-style2 {
            width: 177px;
        }
        .auto-style8 {
            width: 179px;
        }
        .auto-style9 {
            width: 76px;
        }

        .auto-style11 {
            width: 79px;
        }

        </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id ="Contenedor">
    
                <table class="auto-style1">
                    <tr>
                        <td>
                            <dx:ASPxMenu ID="ASPxMenu1" runat="server" EnableTheming="True" Theme="Metropolis" Width="100%">
                                <Items>
                                    <dx:MenuItem NavigateUrl="~/PreviousTaskWFrm.aspx" Text="Atras">
                                    </dx:MenuItem>
                                    <dx:MenuItem NavigateUrl="~/SplashWFrm.aspx" Text="Salir">
                                    </dx:MenuItem>
                                </Items>
                            </dx:ASPxMenu>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ScriptManager ID="ScriptManager1" runat="server">
                            </asp:ScriptManager>
                        </td>
                    </tr>
                    <tr>
                        <td>
                    
                    
    
                    
                    
                    
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <table class="dxflInternalEditorTable_Metropolis">
                                <tr>
                                    <td class="auto-style7">
                                        <dx:ASPxLabel ID="ASPxLabel10" runat="server" Text="Unidad Negocio" Theme="Metropolis">
                                        </dx:ASPxLabel>
                                        <dx:ASPxComboBox ID="cmbUnidadNegocio0" runat="server" DataSourceID="SqlDataSourceUnidadNegocio" EnableTheming="True" SelectedIndex="0" TextField="Nombre" Theme="Metropolis" ValueField="ID" ValueType="System.Int32" AutoPostBack="True">
                                        <ClientSideEvents EndCallback="OnEndCallback" Init="OnComboBoxInit" />
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td class="auto-style2">
                                        <dx:ASPxLabel ID="ASPxLabel11" runat="server" Text="Producto" Theme="Metropolis">
                                        </dx:ASPxLabel>
                                        <dx:ASPxComboBox ID="cmbProductoVegetal0" runat="server" AutoPostBack="True" DataSourceID="SqlDataSourceProductoVegetal" EnableTheming="True" SelectedIndex="0" TextField="Nombre" Theme="Metropolis" ValueField="ID" ValueType="System.Int32">
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td class="auto-style8">
                                        <dx:ASPxLabel ID="ASPxLabel14" runat="server" Text="Variedad" Theme="Metropolis">
                                        </dx:ASPxLabel>
                                        <dx:ASPxComboBox ID="cmbVariedad0" runat="server" DataSourceID="SqlDataSourceVariedad" EnableTheming="True" SelectedIndex="0" TextField="Nombre" Theme="Metropolis" ValueField="ID" ValueType="System.Int32">
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td class="auto-style9">
                                        <dx:ASPxLabel ID="ASPxLabel12" runat="server" Text="Semana Ini" Theme="Metropolis">
                                        </dx:ASPxLabel>
                                        <dx:ASPxComboBox ID="cmbSemanaIni0" runat="server" DataSourceID="SqlDataSourceSemIni" EnableTheming="True" SelectedIndex="0" TextField="Nombre" Theme="Metropolis" ValueField="ID" ValueType="System.Int32" Width="70px">
                                        <ClientSideEvents EndCallback="OnEndCallback" Init="OnComboBoxInit" />
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td class="auto-style11">
                                        <dx:ASPxLabel ID="ASPxLabel13" runat="server" Text="Semana Fin" Theme="Metropolis">
                                        </dx:ASPxLabel>
                                        <dx:ASPxComboBox ID="cmbSemanaFin0" runat="server" DataSourceID="SqlDataSourceSemFin" EnableTheming="True" SelectedIndex="0" TextField="Nombre" Theme="Metropolis" ValueField="ID" ValueType="System.Int32" Width="70px">
                                        <ClientSideEvents EndCallback="OnEndCallback" Init="OnComboBoxInit" />
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td class="auto-style8">
                                        <dx:ASPxLabel ID="lblActividad0" runat="server" Text="Grupo" Theme="Metropolis">
                                        </dx:ASPxLabel>
                                        <dx:ASPxComboBox ID="cmbActividad0" runat="server" DataSourceID="SqlDataSourceActividad" EnableTheming="True" SelectedIndex="0" TextField="Name" Theme="Metropolis" ValueField="ActivityGroupID" ValueType="System.Int32" AutoPostBack="True">
                                        <ClientSideEvents EndCallback="OnEndCallback" Init="OnComboBoxInit" />
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td class="auto-style8">
                                        <dx:ASPxLabel ID="ASPxLabel16" runat="server" Text="Actividad" Theme="Metropolis">
                                        </dx:ASPxLabel>
                                        <dx:ASPxComboBox ID="cmbActividadEspecifica" runat="server" DataSourceID="SqlDataSourceActiviadEspec" EnableTheming="True" SelectedIndex="0" TextField="Name" Theme="Metropolis" ValueField="Name">
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td>
                                        <dx:ASPxLabel ID="ASPxLabel15" runat="server" Text="Bloque" Theme="Metropolis">
                                        </dx:ASPxLabel>
                                        <dx:ASPxComboBox ID="cmbBloque" runat="server" DataSourceID="SqlDataSourceBloque" EnableTheming="True" SelectedIndex="0" TextField="Codigo" Theme="Metropolis" ValueField="ID" ValueType="System.Int32" Width="70px" AutoPostBack="True">
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td> 
                                        <script type="text/javascript">
        var textSeparator = ";";
        function updateText() {
            var selectedItems = checkListBox.GetSelectedItems();
            checkComboBox.SetText(getSelectedItemsText(selectedItems));
        }
        function synchronizeListBoxValues(dropDown, args) {
            checkListBox.UnselectAll();
            var texts = dropDown.GetText().split(textSeparator);
            var values = getValuesByTexts(texts);
            checkListBox.SelectValues(values);
            updateText(); // for remove non-existing texts
        }
        function getSelectedItemsText(items) {
            var texts = [];
            for (var i = 0; i < items.length; i++) 
                    texts.push(items[i].text);
            return texts.join(textSeparator);
        }
        function getValuesByTexts(texts) {
            var actualValues = [];
            var item;
            for(var i = 0; i < texts.length; i++) {
                item = checkListBox.FindItemByText(texts[i]);
                if(item != null)
                    actualValues.push(item.value);
            }
            return actualValues;
        }
    </script>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    
                    
                    
                    
                    
                    
                        </td>
                    </tr>
                    <tr>
                        <td>
        <asp:SqlDataSource ID="SqlDataSourceAppointment" runat="server" ConnectionString="<%$ ConnectionStrings:PlannerTask %>" SelectCommand="sp_GetInfoMappingsTask" SelectCommandType="StoredProcedure" UpdateCommand="EXEC sp_UpdatePlannerTask @UniqueID, @StarlDate, @ReminderInfo">
            <SelectParameters>
                <asp:ControlParameter ControlID="cmbSemanaIni0" DefaultValue="0" Name="prmSemanaINI" PropertyName="Value" Type="Int32" />
                <asp:ControlParameter ControlID="cmbSemanaFin0" DefaultValue="0" Name="prmSemanaFIN" PropertyName="Value" Type="Int32" />
                <asp:ControlParameter ControlID="cmbUnidadNegocio0" DefaultValue="0" Name="prmUnidadNegocio" PropertyName="Value" Type="Int32" />
                <asp:ControlParameter ControlID="cmbActividad0" DefaultValue="0" Name="prmActividad" PropertyName="Value" Type="Int32" />
                <asp:ControlParameter ControlID="cmbProductoVegetal0" DefaultValue="0" Name="prmProductoVegetal" PropertyName="Value" Type="Int32" />
                <asp:ControlParameter ControlID="cmbVariedad0" DefaultValue="0" Name="prmVariedad" PropertyName="Value" Type="Int32" />
                <asp:ControlParameter ControlID="cmbBloque" DefaultValue="0" Name="prmBloque" PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="cmbActividadEspecifica" DefaultValue="TODAS" Name="prmActividadEspecifica" PropertyName="Value" Type="String" />
            </SelectParameters>
            <UpdateParameters>
                <asp:SessionParameter DefaultValue="" Name="ReminderInfo" SessionField="IDs" />
                <asp:SessionParameter DefaultValue="0" Name="UniqueID" SessionField="UniqueID" />
                <asp:SessionParameter DefaultValue="01/01/1900" Name="StarlDate" SessionField="FinalDate" />
            </UpdateParameters>
        </asp:SqlDataSource>
    
                    <asp:SqlDataSource ID="SqlDataSourceProductoVegetal" runat="server" ConnectionString="<%$ ConnectionStrings:PlannerTask %>" SelectCommand="SELECT ID = 0, Nombre = 'TODAS' 
UNION ALL
SELECT PlantProductID AS ID, PlantProductName AS Nombre FROM PlantProducts
WHERE FlagActive = 1 
ORDER BY ID"></asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSourceVariedad" runat="server" ConnectionString="<%$ ConnectionStrings:PlannerTask %>" SelectCommand="SELECT ID = 0, Nombre = 'TODAS'
UNION ALL
SELECT 
   ProductID AS ID,
   UPPER(Product) AS Nombre 
FROM Products
WHERE PlantProductID = @prmPlantProductID
ORDER BY ID">
                        
                        <SelectParameters>
                            <asp:ControlParameter ControlID="cmbProductoVegetal0" DefaultValue="0" Name="prmPlantProductID" PropertyName="Value" />
                        </SelectParameters>
                    </asp:SqlDataSource>
    
        <asp:SqlDataSource ID="SqlDataSourceResources" runat="server" ConnectionString="<%$ ConnectionStrings:PlannerTask %>" SelectCommand="SELECT  
  ResourceID = BusinessUnitID, 
  Caption = UPPER(Name), 
  Color = '',  
  Image = '' 
FROM BusinessUnit 
WHERE BusinessUnitID = @prmUN AND ActiveFlag = 1">
            <SelectParameters>
                <asp:ControlParameter ControlID="cmbUnidadNegocio0" DefaultValue="0" Name="prmUN" PropertyName="Value" />
            </SelectParameters>
                            </asp:SqlDataSource>
    
                            <asp:SqlDataSource ID="SqlDataSourceActiviadEspec" runat="server" ConnectionString="<%$ ConnectionStrings:PlannerTask %>" SelectCommand="SELECT ActivityId = -1, Name = 'TODAS' UNION ALL SELECT ActivityId, UPPER(Name) AS Name FROM Planner_Activities WHERE ActivityGroupID = @prmGrupoAct">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="cmbActividad0" DefaultValue="-1" Name="prmGrupoAct" PropertyName="Value" />
                                </SelectParameters>
                            </asp:SqlDataSource>
    
                    <asp:SqlDataSource ID="SqlDataSourceUnidadNegocio" runat="server" ConnectionString="<%$ ConnectionStrings:PlannerTask %>" SelectCommand="     SELECT ID = 0, Nombre = 'TODAS' 
	UNION ALL 
	SELECT 
	   BusinessUnitID AS ID, 
	   Name AS Nombre 
	FROM BusinessUnit 
	WHERE ActiveFlag = 1 AND BusinessUnitID IN (571, 460,462,464,469,474,500,569,570) ORDER BY ID"></asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSourceActividad" runat="server" ConnectionString="<%$ ConnectionStrings:PlannerTask %>" SelectCommand="SELECT -1 AS ActivityGroupID, Name = 'TODAS' UNION ALL SELECT ActivityGroupID, UPPER(Name) AS Nombre FROM Planner_ActivityGroup ORDER BY ActivityGroupID"></asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSourceSemFin" runat="server" ConnectionString="<%$ ConnectionStrings:PlannerTask %>" SelectCommand="SELECT ID, Nombre =CONVERT(VARCHAR(10),Anio) + CASE WHEN Nombre = '1' THEN '01'
                                                    WHEN Nombre = '2' THEN '02'
													WHEN Nombre = '3' THEN '03'
													WHEN Nombre = '4' THEN '04'
													WHEN Nombre = '5' THEN '05'
													WHEN Nombre = '6' THEN '06'
													WHEN Nombre = '7' THEN '07'
													WHEN Nombre = '8' THEN '08'
													WHEN Nombre = '9' THEN '09' ELSE Nombre END FROM Semanas WHERE Anio &gt;= YEAR(GETDATE()) ORDER BY Anio">
                    </asp:SqlDataSource>
                            <asp:SqlDataSource ID="SqlDataSourceListaCamas" runat="server" ConnectionString="<%$ ConnectionStrings:PlannerTask %>" SelectCommand="SELECT ID = -1, Nombre = 'TODAS' UNION ALL SELECT ID, Nombre FROM Bed WHERE IDInvernaderos = @prmIDInvernaderos">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="cmbBloque" DefaultValue="-1" Name="prmIDInvernaderos" PropertyName="Value" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                            <asp:SqlDataSource ID="SqlDataSourceBloque" runat="server" ConnectionString="<%$ ConnectionStrings:PlannerTask %>" SelectCommand="SELECT ID = -1, Codigo = 'TODOS'
UNION ALL
SELECT ID, Codigo FROM fn_getGreenHousebyBusinessUnit(@prmUnidadNegocio)">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="cmbUnidadNegocio0" DefaultValue="0" Name="prmUnidadNegocio" PropertyName="Value" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSourceSemIni" runat="server" ConnectionString="<%$ ConnectionStrings:PlannerTask %>" SelectCommand="SELECT ID, Nombre =CONVERT(VARCHAR(10),Anio) + CASE WHEN Nombre = '1' THEN '01'
                                                    WHEN Nombre = '2' THEN '02'
													WHEN Nombre = '3' THEN '03'
													WHEN Nombre = '4' THEN '04'
													WHEN Nombre = '5' THEN '05'
													WHEN Nombre = '6' THEN '06'
													WHEN Nombre = '7' THEN '07'
													WHEN Nombre = '8' THEN '08'
													WHEN Nombre = '9' THEN '09' ELSE Nombre END FROM Semanas WHERE Anio &gt;= YEAR(GETDATE()) ORDER BY Anio"></asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Cargar" Theme="Metropolis" Width="100%">
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <dxwschs:ASPxScheduler ID="ASPxScheduler1" runat="server" ActiveViewType="Month" AppointmentDataSourceID="SqlDataSourceAppointment" ClientIDMode="AutoID" DataMember="" DataSourceID="" ResourceDataSourceID="SqlDataSourceResources" Start="2018-12-09" Theme="SoftOrange" Width="100%">
                                <Storage EnableReminders="False">
                                    <Appointments AutoRetrieveId="True">
                                        <Mappings AllDay="AllDay" AppointmentId="UniqueID" Description="Descripcion" End="EndDate" Location="Location" RecurrenceInfo="RecurrenceInfo" ReminderInfo="ReminderInfo" ResourceId="ResourceID" Start="StarDate" Subject="Subject" Type="Type" />
                                    </Appointments>
                                    <Resources>
                                        <Mappings Caption="Caption" Color="Color" Image="Image" ResourceId="ResourceID" />
                                    </Resources>
                                </Storage>
                                <Views>
<DayView Enabled="False"><TimeRulers>
<cc1:TimeRuler></cc1:TimeRuler>
</TimeRulers>

<AppointmentDisplayOptions ColumnPadding-Left="2" ColumnPadding-Right="4"></AppointmentDisplayOptions>
</DayView>

<WorkWeekView Enabled="False"><TimeRulers>
<cc1:TimeRuler></cc1:TimeRuler>
</TimeRulers>

<AppointmentDisplayOptions ColumnPadding-Left="2" ColumnPadding-Right="4"></AppointmentDisplayOptions>
</WorkWeekView>

                                    <WeekView Enabled="False">
                                    </WeekView>
                                    <MonthView WeekCount="4">
                                    </MonthView>
                                    <TimelineView Enabled="False">
                                    </TimelineView>

                                    <FullWeekView>
                                        <TimeRulers>
<cc1:TimeRuler></cc1:TimeRuler>
</TimeRulers>

<AppointmentDisplayOptions ColumnPadding-Left="2" ColumnPadding-Right="4"></AppointmentDisplayOptions>
                                    </FullWeekView>
                                    <AgendaView Enabled="False">
                                    </AgendaView>
                                </Views>
                                <OptionsBehavior ShowViewSelector="False" />
                            </dxwschs:ASPxScheduler>
                        </td>
                    </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
