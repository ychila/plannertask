﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class SchedulingWFrm
    
    '''<summary>
    '''Control form1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm
    
    '''<summary>
    '''Control ASPxMenu1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ASPxMenu1 As Global.DevExpress.Web.ASPxMenu
    
    '''<summary>
    '''Control ScriptManager1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager
    
    '''<summary>
    '''Control UpdatePanel2.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents UpdatePanel2 As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''Control ASPxLabel10.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ASPxLabel10 As Global.DevExpress.Web.ASPxLabel
    
    '''<summary>
    '''Control cmbUnidadNegocio0.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmbUnidadNegocio0 As Global.DevExpress.Web.ASPxComboBox
    
    '''<summary>
    '''Control ASPxLabel11.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ASPxLabel11 As Global.DevExpress.Web.ASPxLabel
    
    '''<summary>
    '''Control cmbProductoVegetal0.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmbProductoVegetal0 As Global.DevExpress.Web.ASPxComboBox
    
    '''<summary>
    '''Control ASPxLabel14.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ASPxLabel14 As Global.DevExpress.Web.ASPxLabel
    
    '''<summary>
    '''Control cmbVariedad0.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmbVariedad0 As Global.DevExpress.Web.ASPxComboBox
    
    '''<summary>
    '''Control ASPxLabel12.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ASPxLabel12 As Global.DevExpress.Web.ASPxLabel
    
    '''<summary>
    '''Control cmbSemanaIni0.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmbSemanaIni0 As Global.DevExpress.Web.ASPxComboBox
    
    '''<summary>
    '''Control ASPxLabel13.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ASPxLabel13 As Global.DevExpress.Web.ASPxLabel
    
    '''<summary>
    '''Control cmbSemanaFin0.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmbSemanaFin0 As Global.DevExpress.Web.ASPxComboBox
    
    '''<summary>
    '''Control lblActividad0.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblActividad0 As Global.DevExpress.Web.ASPxLabel
    
    '''<summary>
    '''Control cmbActividad0.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmbActividad0 As Global.DevExpress.Web.ASPxComboBox
    
    '''<summary>
    '''Control ASPxLabel16.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ASPxLabel16 As Global.DevExpress.Web.ASPxLabel
    
    '''<summary>
    '''Control cmbActividadEspecifica.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmbActividadEspecifica As Global.DevExpress.Web.ASPxComboBox
    
    '''<summary>
    '''Control ASPxLabel15.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ASPxLabel15 As Global.DevExpress.Web.ASPxLabel
    
    '''<summary>
    '''Control cmbBloque.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmbBloque As Global.DevExpress.Web.ASPxComboBox
    
    '''<summary>
    '''Control SqlDataSourceAppointment.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents SqlDataSourceAppointment As Global.System.Web.UI.WebControls.SqlDataSource
    
    '''<summary>
    '''Control SqlDataSourceProductoVegetal.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents SqlDataSourceProductoVegetal As Global.System.Web.UI.WebControls.SqlDataSource
    
    '''<summary>
    '''Control SqlDataSourceVariedad.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents SqlDataSourceVariedad As Global.System.Web.UI.WebControls.SqlDataSource
    
    '''<summary>
    '''Control SqlDataSourceResources.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents SqlDataSourceResources As Global.System.Web.UI.WebControls.SqlDataSource
    
    '''<summary>
    '''Control SqlDataSourceActiviadEspec.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents SqlDataSourceActiviadEspec As Global.System.Web.UI.WebControls.SqlDataSource
    
    '''<summary>
    '''Control SqlDataSourceUnidadNegocio.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents SqlDataSourceUnidadNegocio As Global.System.Web.UI.WebControls.SqlDataSource
    
    '''<summary>
    '''Control SqlDataSourceActividad.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents SqlDataSourceActividad As Global.System.Web.UI.WebControls.SqlDataSource
    
    '''<summary>
    '''Control SqlDataSourceSemFin.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents SqlDataSourceSemFin As Global.System.Web.UI.WebControls.SqlDataSource
    
    '''<summary>
    '''Control SqlDataSourceListaCamas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents SqlDataSourceListaCamas As Global.System.Web.UI.WebControls.SqlDataSource
    
    '''<summary>
    '''Control SqlDataSourceBloque.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents SqlDataSourceBloque As Global.System.Web.UI.WebControls.SqlDataSource
    
    '''<summary>
    '''Control SqlDataSourceSemIni.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents SqlDataSourceSemIni As Global.System.Web.UI.WebControls.SqlDataSource
    
    '''<summary>
    '''Control ASPxButton1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ASPxButton1 As Global.DevExpress.Web.ASPxButton
    
    '''<summary>
    '''Control ASPxScheduler1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ASPxScheduler1 As Global.DevExpress.Web.ASPxScheduler.ASPxScheduler
End Class
