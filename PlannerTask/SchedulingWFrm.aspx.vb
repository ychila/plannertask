﻿Imports DevExpress.Web.ASPxScheduler
Imports DevExpress.XtraScheduler

Public Class SchedulingWFrm
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        InicializarConexion()

        If (Not IsPostBack) Then

            Me.ASPxScheduler1.Start = Date.Now

            SqlDataSourceSemIni.FilterExpression = "ID = " & Me.Session("SemIni").ToString & ""
            SqlDataSourceSemFin.FilterExpression = "ID = " & Me.Session("SemFin").ToString & ""
            SqlDataSourceActividad.FilterExpression = "ActivityGroupID = " & Me.Session("Actividad").ToString & ""
            SqlDataSourceUnidadNegocio.FilterExpression = "ID = " & Me.Session("UN").ToString & ""

        End If


        If Not IsPostBack = True Then




        End If
    End Sub

    Protected Sub ASPxButton1_Click(sender As Object, e As EventArgs) Handles ASPxButton1.Click
        SqlDataSourceAppointment.DataBind()
        ASPxScheduler1.DataBind()
    End Sub

    Private Sub cmbProductoVegetal0_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbProductoVegetal0.SelectedIndexChanged
        cmbVariedad0.SelectedIndex = 0
    End Sub

    Private Sub cmbUnidadNegocio0_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbUnidadNegocio0.SelectedIndexChanged

        Me.SqlDataSourceUnidadNegocio.ConnectionString = strConexion
        Me.SqlDataSourceUnidadNegocio.SelectCommand = "SELECT ID = 0, Nombre = 'TODAS' UNION ALL SELECT BusinessUnitID AS ID, Name AS Nombre FROM BusinessUnit WHERE ActiveFlag = 1 AND BusinessUnitID IN (571, 460,462,464,469,474,500,569,570) ORDER BY ID"
        Me.cmbUnidadNegocio0.DataBind()
        Me.SqlDataSourceUnidadNegocio.DataBind()

        cmbBloque.SelectedIndex = 0



    End Sub

    Private Sub cmbActividad0_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbActividad0.SelectedIndexChanged

        Me.SqlDataSourceActividad.ConnectionString = strConexion
        Me.SqlDataSourceActividad.SelectCommand = "SELECT -1 AS ActivityGroupID, Name = 'TODAS' UNION ALL SELECT ActivityGroupID, UPPER(Name) AS Nombre FROM Planner_ActivityGroup ORDER BY ActivityGroupID"
        Me.cmbActividad0.DataBind()
        Me.SqlDataSourceActividad.DataBind()

        cmbActividadEspecifica.SelectedIndex = 0
    End Sub

    Private Sub ASPxScheduler1_AppointmentRowUpdating(sender As Object, e As ASPxSchedulerDataUpdatingEventArgs) Handles ASPxScheduler1.AppointmentRowUpdating
        Me.Session("FinalDate") = e.NewValues("StarDate")
        Me.Session("TaskID") = e.NewValues("UniqueID")
        Me.Session("IDs") = e.OldValues("ReminderInfo")

        ASPxScheduler1.DataBind()
        ASPxScheduler1.Storage.RefreshData()

    End Sub


    Private Sub ASPxScheduler1_AppointmentsChanged(sender As Object, e As PersistentObjectsEventArgs) Handles ASPxScheduler1.AppointmentsChanged
        ASPxScheduler1.DataBind()
        ASPxScheduler1.Storage.RefreshData()
    End Sub

    Private Sub cmbSemanaIni0_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbSemanaIni0.SelectedIndexChanged
        Me.SqlDataSourceSemIni.ConnectionString = strConexion
        Me.SqlDataSourceSemIni.SelectCommand = "SELECT ID, Nombre =CONVERT(VARCHAR(10),Anio) + CASE WHEN Nombre = '1' THEN '01'
                                                    WHEN Nombre = '2' THEN '02'
													WHEN Nombre = '3' THEN '03'
													WHEN Nombre = '4' THEN '04'
													WHEN Nombre = '5' THEN '05'
													WHEN Nombre = '6' THEN '06'
													WHEN Nombre = '7' THEN '07'
													WHEN Nombre = '8' THEN '08'
													WHEN Nombre = '9' THEN '09' ELSE Nombre END FROM Semanas WHERE Anio >= YEAR(GETDATE()) ORDER BY Anio"
        Me.cmbSemanaIni0.DataBind()
        Me.SqlDataSourceSemIni.DataBind()
    End Sub

    Private Sub cmbSemanaFin0_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbSemanaFin0.SelectedIndexChanged
        Me.SqlDataSourceSemFin.ConnectionString = strConexion
        Me.SqlDataSourceSemFin.SelectCommand = "SELECT ID, Nombre =CONVERT(VARCHAR(10),Anio) + CASE WHEN Nombre = '1' THEN '01'
                                                    WHEN Nombre = '2' THEN '02'
													WHEN Nombre = '3' THEN '03'
													WHEN Nombre = '4' THEN '04'
													WHEN Nombre = '5' THEN '05'
													WHEN Nombre = '6' THEN '06'
													WHEN Nombre = '7' THEN '07'
													WHEN Nombre = '8' THEN '08'
													WHEN Nombre = '9' THEN '09' ELSE Nombre END FROM Semanas WHERE Anio >= YEAR(GETDATE()) ORDER BY Anio"
        Me.cmbSemanaFin0.DataBind()
        Me.SqlDataSourceSemFin.DataBind()
    End Sub
End Class