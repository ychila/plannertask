﻿Imports System.Data.SqlClient

Public Class SplashWFrm
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormsAuthentication.SignOut()

        Me.txtLogin.Focus()
        InicializarConexion()
    End Sub
    Public Function encryptPassword(ByVal password As String) As String

        Dim strResult As String = String.Empty
        Dim strSaltedPassword As String = "D639301F009442858D725B3B536B9E7E" & password
        Dim shaM As New System.Security.Cryptography.SHA1Managed()
        Dim HashedPassword As Byte() = shaM.ComputeHash(System.Text.Encoding.ASCII.GetBytes(strSaltedPassword))

        Dim b As Byte
        For Each b In HashedPassword
            strResult &= Hex(b)
        Next


        Return strResult

    End Function
    Protected Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        GetIDUsr()

        If Me.Session("IDUsr") <> 0 Then

            FormsAuthentication.RedirectFromLoginPage(Me.Session("NombreUsr"), False)
            Me.Response.Redirect("PreviousTaskWFrm.aspx")

        End If
    End Sub
    Private Sub GetIDUsr()
        Dim cnn As New SqlConnection(strConexion)
        Dim Comando As New SqlCommand
        Dim strComando As String = ""
        Dim Lector As SqlDataReader
        On Error GoTo ControlaError

        Me.txtLogin.Text = Me.txtLogin.Text.Replace("--", "")
        Me.txtLogin.Text = Me.txtLogin.Text.Replace("*/", "")
        Me.txtLogin.Text = Me.txtLogin.Text.Replace("/*", "")
        Me.txtLogin.Text = Me.txtLogin.Text.Replace("UPDATE", "")
        Me.txtLogin.Text = Me.txtLogin.Text.Replace("INSERT", "")

        Me.txtPassword.Text = Me.txtPassword.Text.Replace("--", "")
        Me.txtPassword.Text = Me.txtPassword.Text.Replace("*/", "")
        Me.txtPassword.Text = Me.txtPassword.Text.Replace("/*", "")
        Me.txtPassword.Text = Me.txtPassword.Text.Replace("UPDATE", "")
        Me.txtPassword.Text = Me.txtPassword.Text.Replace("INSERT", "")


        strComando = "EXEC sp_GetUserPlannerAccess '" & Me.txtLogin.Text & "', '" & encryptPassword(Me.txtPassword.Text) & "'"

        With Comando
            .CommandText = strComando
            .CommandTimeout = 0
            .Connection = cnn
            .Connection.Open()

            Lector = .ExecuteReader(CommandBehavior.CloseConnection)

        End With

        If Lector.Read Then
            Me.Session("IDUsr") = Lector.GetValue(0)
            Me.Session("NombreUsr") = Lector.GetValue(1)

        Else
            Me.Session("IDUsr") = 0
            Me.Session("NombreUsr") = ""

        End If
        Lector.Close()

        Exit Sub
ControlaError:
        Dim strError As String = Err.Description

    End Sub

End Class